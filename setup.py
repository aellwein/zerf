from setuptools import setup

setup(
    name='zerf',
    version='0.1.0+git',
    description='Zeiterfassungsgenerator (Excel)',
    author='Alex Ellwein',
    author_email='alex.ellwein@gmail.com',
    url='TBD',
    license='MIT',
    install_requires=[
        "xlsxwriter==3.1.9",
        "ics==0.7.2",
    ],
    packages=['zerf'],
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'zerf=zerf.zeiterfassung:main',
        ],
    },
)
