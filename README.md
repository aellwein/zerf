zerf (a.k.a. Zeiterfassung)
===========================

A small Python script to generate time recordings Excel (Zeiterfassung)
including national (currently Baden-Württemberg only) and public holidays.

Usage
-----

* You can get pre-generated XLSX file from ``pre-generated`` folder, or
* create a python virtualenv, then activate it:
  ```
  python3 -m venv .venv
  source .venv/bin/activate
  ```
* run ``pip install -r requirements.txt``
* run ``zerf --year <year_to_generate_for>``, 
  also you can run ``zerf`` to see the possible usage options.
* Generated XLSX file is located in the same directory.
