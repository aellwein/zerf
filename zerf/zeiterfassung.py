import argparse
import datetime
import locale
import os

import ics
import xlsxwriter


class HolidayCalendar(object):
    holidays = dict()
    pub_holidays = dict()

    def __init__(self, year):
        with open(os.path.join(os.path.dirname(__file__), 'calendar', f'holidays_{year}.ics'), 'rb') as f:
            cal = ics.Calendar(''.join([l.decode() for l in f.readlines()]))
            for ev in cal.events:
                start = ev.begin.date()
                end = ev.end.date()
                while start <= end:
                    self.holidays[start] = ev.name
                    start += datetime.timedelta(days=1)

        with open(os.path.join(os.path.dirname(__file__), 'calendar', f'BW_{year}.ics'), 'rb') as f:
            cal = ics.Calendar(''.join([l.decode() for l in f.readlines()]))
            for ev in cal.events:
                start = ev.begin.date()
                self.pub_holidays[start] = ev.name


def days(year: int):
    day = datetime.date(year, 1, 1)
    end_day = datetime.date(year, 12, 31)
    while day.year == end_day.year:
        yield day
        day = day + datetime.timedelta(days=1)


def fmt(wb: xlsxwriter.Workbook, wb_init=None, **kwargs):
    def align(f, a):
        f.set_align(a)
        return f

    def bottom(f, b):
        f.set_bottom(b)
        return f

    def fg_color(f, c):
        f.set_fg_color(c)
        return f

    def bg_color(f, c):
        f.set_bg_color(c)
        return f

    def border(f, b):
        f.set_border(b)
        return f

    def bold(f, b):
        f.set_bold(b)
        return f

    funcs = {
        'align': align,
        'bottom': bottom,
        'fg_color': fg_color,
        'bg_color': bg_color,
        'border': border,
        'bold': bold,
    }
    f = wb.add_format(*wb_init) if wb_init else wb.add_format()

    for k, v in kwargs.items():
        if k in funcs:
            f = funcs[k](f, v)
    return f


def days_of_month(m: int, y: int) -> int:
    d = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    days = d[m - 1]
    if m == 2:
        if y % 4 == 0:
            days += 1
    return days


def generate(year: int, cal: HolidayCalendar, _locale: str = None):
    if _locale:
        locale.setlocale(locale.LC_TIME, _locale)

    wb = xlsxwriter.Workbook(f'calendar_{year}.xlsx', {'date_1904': True})
    sheets = {}

    for day in days(year):
        sheetname = day.strftime('%B %y')
        if day.day == 1 and day.month not in sheets:
            ws = wb.add_worksheet(sheetname)
            sheets[day.month] = ws
            row = 1
        ws = sheets[day.month]

        weekend = day.weekday() == 5 or day.weekday() == 6

        if weekend:
            weekend_fmt = {'bg_color': '#eab28b'}
            if day in cal.holidays:
                ws.write(row, 7, cal.holidays[day], fmt(wb, align='left', bold=True))
            elif day in cal.pub_holidays:
                ws.write(row, 7, cal.pub_holidays[day], fmt(wb, align='left', bold=True))
        else:
            weekend_fmt = {}
            if day in cal.holidays:
                weekend_fmt = {'bg_color': '#fdf2d0'}
                ws.write(row, 7, cal.holidays[day], fmt(wb, align='left', bold=True))
            elif day in cal.pub_holidays:
                weekend_fmt = {'bg_color': '#ee7b98'}
                ws.write(row, 7, cal.pub_holidays[day], fmt(wb, align='left', bold=True))

        # headers
        ws.write('A1', 'Datum', fmt(wb, align='right', bottom=1))
        ws.write('B1', 'Start', fmt(wb, align='right', bottom=1))
        ws.write('C1', 'Ende', fmt(wb, align='right', bottom=1))
        ws.write('D1', 'Pause', fmt(wb, align='right', bottom=1))
        ws.write('E1', 'Ist', fmt(wb, align='right', bottom=1))
        ws.write('F1', 'Soll', fmt(wb, align='right', bottom=1))
        ws.write('G1', 'Differenz', fmt(wb, align='right', bottom=1))
        ws.write('H1', 'Kommentar', fmt(wb, align='center', bottom=1))

        ws.write(row, 0, day.strftime('%x'), fmt(wb, align='right', **weekend_fmt))
        ws.write_blank(row, 1, None, fmt(wb, wb_init=({'num_format': '[h]:mm'},), align='right', **weekend_fmt))
        ws.write_blank(row, 2, None, fmt(wb, wb_init=({'num_format': '[h]:mm'},), align='right', **weekend_fmt))
        ws.write_blank(row, 3, None, fmt(wb, wb_init=({'num_format': '[h]:mm'},), align='right', **weekend_fmt))
        ws.write_formula(row, 4, f'=SUM(C{row+1}-B{row+1}-D{row+1})', fmt(wb, wb_init=({'num_format': '[h]:mm'},), align='right', **weekend_fmt))
        ws.write_formula(row, 6, f'=SUM(E{row+1}-F{row+1})', fmt(wb, wb_init=({'num_format': '[h]:mm'},), align='right'))

        if weekend or day in cal.pub_holidays:
            ws.write_datetime(row, 5, datetime.timedelta(hours=0), fmt(wb, wb_init=({'num_format': 'hh:mm'},), align='right', **weekend_fmt))
        else:
            ws.write_datetime(row, 5, datetime.timedelta(hours=8), fmt(wb, wb_init=({'num_format': 'hh:mm'},), align='right', **weekend_fmt))

        row += 1

    for month, ws in sheets.items():
        ws.write('E35', 'Gesamt Ist', fmt(wb, align='right', border=1))
        ws.write('F35', 'Gesamt Soll', fmt(wb, align='right', border=1))
        ws.write('G35', 'Differenz', fmt(wb, align='right', border=1))
        ws.write_formula('E36', f'=SUM(E2:E{days_of_month(month, year)+1})', fmt(wb, wb_init=({'num_format': '[h]:mm'},), align='right', border=1))
        ws.write_formula('F36', f'=SUM(F2:F{days_of_month(month, year)+1})', fmt(wb, wb_init=({'num_format': '[h]:mm'},), align='right', border=1))
        ws.write_formula('G36', f'=SUM(E36-F36)', fmt(wb, wb_init=({'num_format': '[h]:mm:ss'},), align='right', border=1))

    wb.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--year', type=int, required=True, default=datetime.datetime.now().year, help='year for which to generate Excel file.')
    parser.add_argument('--locale', type=str, default='de_DE', help='locale to use for date/time functions, e.g. de_DE')
    args = parser.parse_args()

    generate(args.year, HolidayCalendar(str(args.year)), args.locale)
